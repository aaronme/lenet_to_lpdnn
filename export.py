# Export fake quantized PACT network to an annotated or quantized ONNX graph
import os
import torch
import onnx
from lenet5 import LeNet5, load_MNIST_dataset, MNIST_stats, asymmetric_zero_point
import quantlib.backends.lpdnn as lpdnn
#old
from quantlib.algorithms.qalgorithms.qatalgorithms.pact.quantized_onnx import convert_to_quantized_onnx
#new
from quantlib.backends.lpdnn import QONNXConverter 

# Path to train/test data
path_data = "/scratch/msc22h1/quantlab/systems/MNIST/data/" 

# Needed datasets
valid_set = load_MNIST_dataset(path_data, train=False)
valid_loader = torch.utils.data.DataLoader(valid_set, batch_size=16)

# Load PACT fake-quantized network
net_fq = torch.load("logs/lenet5_fq_maxp.pth")

################################
#           Export             #
################################

# quantized ONNX export
print("Exporting to quantzed onnx...")
x, _ = valid_set[0]
x = x.unsqueeze(0)
input_scale = torch.Tensor([MNIST_stats['quantize']['scale']])
input_zero = torch.Tensor([0])
#input_zero = asymmetric_zero_point(input_scale.item(), MNIST_stats['quantize']['min'], MNIST_stats['quantize']['max'], 0, 255)

# Old converter
#onnx_model = convert_to_quantized_onnx(net_fq, x.shape, torch.Size([1,10]), input_scale, torch.Tensor([input_zero]))
# New converter
qonnx_converter = QONNXConverter(net_fq, x.shape, torch.Size([1,10]), input_scale, input_zero)
onnx_model = qonnx_converter.convert()


filepath = "onnx/lenet5_ql_pact_maxp_new.onnx"
onnx.save(onnx_model, filepath)

onnx.checker.check_model(onnx_model)


"""
# ANNOTATION export
print("Exporting  and annotating...")
lpdnn_exporter = lpdnn.LPDNNExporter()
path = "onnx/"
name = "lenet5_ql_pact_maxp_annotated"
x, _ = valid_set[0]
x = x.unsqueeze(0)
lpdnn_exporter.export_and_annotate(fq_network=net_fq,
                                         input_shape=x.shape,
                                         path=path,
                                         name=name)
"""
"""
# Default pytorch export

onnx_path = "onnx/lenet5_ql_integerised_maxp.onnx"
debug_path = "onnx/debug.onnx"
path = debug_path
print(f"Exporting to {path}")
torch.onnx.export(net_tq.to(torch.device('cpu')),
                      x,
                      path,
                      export_params=True,
                      opset_version=10,
                      do_constant_folding=True)
"""
