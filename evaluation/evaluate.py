#!usr/bin/env python3

import sys
import os
import argparse
import numpy as np
import torch
import torch.nn as nn
import onnx
import onnxruntime as ort

from lenet5 import load_MNIST_dataset, MNIST_stats, MNISTQuantize

path_data = "/scratch/msc22h1/quantlab/systems/MNIST/data/"

def evaluate_model_pytorch(args, dataset: torch.utils.data.Dataset):
    
    batch_size = 64
    loader = torch.utils.data.DataLoader(dataset, batch_size)

    
    model = torch.load(args.model)
    #device = torch.cuda.current_device() if torch.cuda.is_available() else torch.device("cpu")
    device = torch.device("cpu") #TODO: cuda was throwing an error. Check that
    print(f"Device: {device}")
    model = model.to(device)

    print(f"\nEvaluating {args.model} using pytorch on MNIST test set\n-----------------------------------")
    model.eval()
    correct = 0
    for X, y in loader:

        X = X.to(device=device)
        y = y.to(device=device)

        y_pr = torch.argmax(model(X), dim=1)
        correct += (y_pr == y).type(torch.float).sum().item()
    
    acc = (correct/len(loader.dataset))*100
    print(f"Accuracy = {acc}%")


def onnx_create_session(model: str) -> ort.InferenceSession:
    providers = ['CPUExecutionProvider']
    #if torch.cuda.is_available():
    #    providers.insert(0, 'CUDAExecutionProvider')
        
    session = ort.InferenceSession(model, providers=providers)
    input_name = session.get_inputs()[0].name
    output_name = session.get_outputs()[0].name
    
    return session, input_name, output_name

def evaluate_model_onnx(args, dataset: torch.utils.data.Dataset):

    loader = torch.utils.data.DataLoader(dataset, batch_size=1) # onnx model accepts outer dim (batch size) of  1
    inference_session, input_, output_ = onnx_create_session(args.model)
    
    print(f"\nEvaluating {args.model} using onnxruntime on MNIST test set\n-----------------------------------")
    correct = 0
    for x, y in loader:
        x = x.numpy()
        y = y.numpy()
        model_out = inference_session.run([output_], {input_:x})[0]
        pred = np.argmax(model_out)
        correct += (pred == y.item()).astype(float)
    
    acc = (correct/len(loader.dataset))*100
    print(f"Accuracy = {acc}%")


def main():

    parser = argparse.ArgumentParser('Validate onnx or pytorch model on mnist validation set')
    parser.add_argument('-m', '--model', dest='model', type=str, required=True, help='path to .onnx/pytorch model file')
    parser.add_argument('--quantize', required=False, action='store_true', help='Instructs to quantize the inputs')
    parser.add_argument('--fake-quantize', required=False, action='store_true', help='Instructs to fake-quantize the inputs')
    parser.add_argument('--unsigned', required=False, action='store_true', help='Instructs to use unsigned qrange for the inputs')
    parser.add_argument('--asymmetric', required=False, action='store_true', help='Instructs to use asymmetric quantization for the inputs')

    args = parser.parse_args()

    signed = not args.unsigned
    
    dataset = load_MNIST_dataset(path_data, train=False, quantize=args.quantize, fake_quantize=args.fake_quantize, signed=signed, asymmetric=args.asymmetric)

    if args.model.endswith('.pth'):
        evaluate_model_pytorch(args,dataset)
    
    if args.model.endswith('.onnx'):
        evaluate_model_onnx(args,dataset)

if __name__ == '__main__':
    main()
