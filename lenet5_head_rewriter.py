import torch
import torch.nn as nn
import torch.fx as fx
import quantlib.editing.graphs as qg
import quantlib.editing.editing as qe

def last_linear_checker(m: nn.Linear) -> bool:
    return (m.in_features == 1) and (m.out_features == 1)

last_linear_target = 'lin'

name_to_checker = {last_linear_target:last_linear_checker}

class LeNet5Head(nn.Module):
    def __init__(self):
        super(LeNet5Head, self).__init__()

        self.lin = nn.Linear(1,1,bias=False)
        self.eps = qg.nn.EpsTunnel(torch.Tensor([1.0]))

    def forward(self, x):
        x = self.lin(x)
        x = self.eps(x)
        return x


class LeNet5HeadApplier(qe.editors.nnmodules.NNModuleApplier):
    def __init__(self, headpattern: qe.editors.nnmodules.GenericNNModulePattern):
        super(LeNet5HeadApplier, self).__init__(headpattern)

    def _apply(self, g: fx.GraphModule, ap: qe.editors.nnmodules.NodesMap, id_: str) -> fx.GraphModule:
        
        name_to_match_node = self.pattern.name_to_match_node(nodes_map=ap)
        node_lin = name_to_match_node['lin']

        name_to_match_module = self.pattern.name_to_match_module(nodes_map=ap, data_gm=g)
        module_lin = name_to_match_module['lin']
        module_eps = name_to_match_module['eps']

        #assert(node_lin.all_input_nodes) == 1

        # create the new module
        #new_target = id_
        #new_module = nn.Linear(in_features=module_lin.in_features, out_features=module_lin.out_features, bias=module_lin.bias)
        #new_weight = module_lin.weight.data.detach().clone() *  module_eps.eps_out
        #new_module.weight.data = new_weight

        # add the requantised linear operation to the graph
        #g.add_submodule(new_target, new_module)
        #linear_input = next(iter(node_lin.all_input_nodes))
        #with g.graph.inserting_after(linear_input):
        #    new_node = g.grap.call_module(new_target, args=(linear_input,))
        #node_lin.replace_all_uses_with(new_node)

        # delete old operation
        #g.delete_submodule(node_lin.target)
        #g.graph.erase_node(node_lin)
        
        module_eps.set_eps_out(torch.ones_like(module_eps.eps_out))

        return g

class LeNet5HeadRewriter(qe.editors.nnmodules.NNModuleRewriter):
    def __init__(self):
        headwithcheckers = qe.editors.nnmodules.NNModuleWithCheckers(LeNet5Head(), {})
        headpattern = qe.editors.nnmodules.GenericNNModulePattern(qg.fx.quantlib_symbolic_trace, headwithcheckers)

        finder = qe.editors.nnmodules.GenericGraphMatcher(headpattern)
        applier = LeNet5HeadApplier(headpattern)

        super(LeNet5HeadRewriter, self).__init__('LeNet5HeadRewriter', headpattern, finder, applier)
