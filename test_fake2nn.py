import torch
import copy
import onnx
import torch.nn as nn
from lenet5 import LeNet5, load_MNIST_dataset, test_loop

import quantlib.editing.graphs as qg
import quantlib.editing.editing as qe
import quantlib.algorithms as qa
from quantlib.algorithms.qmodules.qmodules.qmodules import _QModule, _QLinear

from quantlib.backends.lpdnn.fake2nn import FakeToNNConverter 
from quantlib.backends.lpdnn import LPDNNExporter, F2FPACTLPDNNConverter 

def print_state(g):
    print(f"PRINTING STATE\n----------------------------------------------------------")
    for node in g.graph.nodes:
        if node.op == "call_module":
            print(f"{node.op} - {node.name} - {node.target} - {g.get_submodule(node.target)} - {type(g.get_submodule(node.target))}")
        else:
            print(f"{node.op} - {node.name} - {node.target}")
        print(f"args: {node.args}, users: {node.users}")

# Dataset
#device = torch.device(torch.cuda.current_device()) if torch.cuda.is_available() else torch.device('cpu')
path_data = "/scratch/msc22h1/quantlab/systems/MNIST/data/"
valid_set = load_MNIST_dataset(path_data, train=False, integerize=False)

# Load PACT fake-quantized network
net_fq = torch.load("logs/lenet5_fq_maxp.pth")


for node in net_fq.graph.nodes:
    if node.op == "call_module":
        m = net_fq.get_submodule(node.target)
        if isinstance(m, _QModule):
            print(f"{m.__class__.__name__} zero = {m.zero}")
            print(f"QRange is_unsigned: {m._qrange.is_unsigned}")
            print(f"QRange is_quasisymmetric: {m._qrange.is_quasisymmetric}")
            print(f"QRange is_symmetric: {m._qrange.is_symmetric}")



""" Old tests

#Test manually
saved_net_fq = copy.deepcopy(net_fq)
#print("saved_net_fq")
#print_state(saved_net_fq)
# Test activation remover
fake2nnconverter = FakeToNNConverter()
net_nn = fake2nnconverter(net_fq)
#print("saved_net_fq")
#print_state(saved_net_fq)
#print("net_nn")
#print_state(net_nn)

#Exporting and annotating
print("Exporting  and annotating...")
lpdnn_exporter = LPDNNExporter()
path = "onnx/"
x, _ = valid_set[0]
x = x.unsqueeze(0)
lpdnn_exporter.export_and_annotate(fq_network=saved_net_fq,
                                         input_shape=x.shape,
                                         path=path,
                                         name="debug")

nodes = onnx_model.graph.node
initializers = onnx_model.graph.initializer
print(initializers)
from quantlib.editing.graphs.fx import FXOpcodeClasses
skipped = 0

for i,node in enumerate(net_nn.graph.nodes):
    if node.op in FXOpcodeClasses.PLACEHOLDER.value or node.op in FXOpcodeClasses.OUTPUT.value:
        skipped += 1
        continue
    if node.op in FXOpcodeClasses.CALL_MODULE.value:
        m = net_nn.get_submodule(node.target)
        class_ = type(m).__name__
        n_onnx = nodes[i - skipped]
        print(f"ONNX: {n_onnx.op_type} --- NN: {class_}")
        print(type(n_onnx.op_type))
    if node.op in FXOpcodeClasses.CALL_FUNCTION.value:
        function = node.target.__name__
        n_onnx = nodes[i - skipped]
        print(f"ONNX: {n_onnx.op_type} --- NN: {function}")
"""
