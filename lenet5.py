# LeNet5 implementation from: https://towardsdatascience.com/implementing-yann-lecuns-lenet-5-in-pytorch-5e05a0911320
# ReLU instead of Tanh for PACT quantization.

import os
import math
import torch
import torch.nn as nn
import torchvision
import torchvision.transforms as transforms

class LeNet5(nn.Module):

    def __init__(self, n_classes: int = 10, pool_type: str = 'MaxPool'):
        super(LeNet5, self).__init__()
        
        self.feature_extractor = self._make_feature_extractor(pool_type)
        self.classifier = self._make_classifier(n_classes)

    def forward(self, x):
        x = self.feature_extractor(x)
        x = torch.flatten(x, 1)#x.view(x.size(0), -1)
        logits = self.classifier(x)
        #probs = F.softmax(logits, dim=1)
        return logits #, probs

    @staticmethod
    def _make_feature_extractor(pool_type: str) -> nn.Sequential:
        modules = []
        modules += [nn.Conv2d(in_channels=1, out_channels=6, kernel_size=5, stride=1, bias=False)]
        modules += [nn.ReLU(inplace=True)]
        if pool_type == 'MaxPool':
            modules += [nn.MaxPool2d(kernel_size=2)]
        elif pool_type == 'AvgPool':
            modules += [nn.AvgPool2d(kernel_size=2, count_include_pad=False)]
        modules += [nn.Conv2d(in_channels=6, out_channels=16, kernel_size=5, stride=1, bias=False)]
        modules += [nn.ReLU(inplace=True)]
        if pool_type == 'MaxPool':
            modules += [nn.MaxPool2d(kernel_size=2)]
        elif pool_type == 'AvgPool':
            modules += [nn.AvgPool2d(kernel_size=2, count_include_pad=False)]
        modules += [nn.Conv2d(in_channels=16, out_channels=120, kernel_size=5, stride=1, bias=False)]
        modules += [nn.ReLU(inplace=True)]

        return nn.Sequential(*modules)

    @staticmethod
    def _make_classifier(n_classes: int) -> nn.Sequential:
        modules = []
        modules += [nn.Linear(in_features=120, out_features=84, bias=False)]
        modules += [nn.ReLU(inplace=True)]
        modules += [nn.Linear(in_features=84, out_features=n_classes, bias=False)]

        return nn.Sequential(*modules)

    @staticmethod
    def init_weights_uniform(m: nn.Module):
        if isinstance(m, nn.Linear) or isinstance(m, nn.Conv2d):
            nn.init.uniform_(m.weight, a=-1.0, b=1.0)




""" Test model class

net = LeNet5(10)
device = torch.device(torch.cuda.current_device() if torch.cuda.is_available() else "cpu")
net = net.to(device=device)
print(net)

x = torch.randn(1,1,32,32).to(device=device)
y = net(x)
print(f"y = {y}, shape = {y.shape}") 
"""

##### Utility functions #####

MNIST_stats = {'normalize': {'mean': (0.1309,), 'std':  (0.3087,)}, 'quantize': {'scale': 0.012703493529675054, 'min': -0.42403626441955566, 'max': 2.815354585647583}}

def get_dataset_range(dataloader: torch.utils.data.DataLoader):
    """Traverse the dataset of dataloader to get the range of input pixels"""
    min_ = 0.0
    max_ = 0.0

    for x, _ in dataloader:
        min_ = min(min_, x.min().item())
        max_ = max(max_, x.max().item())

    return min_, max_

def asymmetric_zero_point(scale: float, rmin: float, rmax: float, qmin: int, qmax: int):
    """Computes zero_point for asymmetric quantization"""
    
    zeroPointFromMin = qmin - rmin / scale
    zeroPointFromMax = qmax - rmax / scale
    zeroPointFromMinError = math.fabs(qmin) + math.fabs(rmin / scale)
    zeroPointFromMaxError = math.fabs(qmax) + math.fabs(rmax / scale)
    initialZeroPoint = zeroPointFromMin
    if zeroPointFromMaxError < zeroPointFromMinError:
        initialZeroPoint = zeroPointFromMax 

    roundedZeroPoint = 0
    if initialZeroPoint < qmin:
        roundedZeroPoint = qmin
    elif initialZeroPoint > qmax:
        roundedZeroPoint = qmax
    else:
        roundedZeroPoint = round(initialZeroPoint)

    return roundedZeroPoint

class MNISTQuantize(transforms.Lambda):
    def __init__(self, scale: float, n_levels: int, signed: bool, asymmetric: bool = False, dequantize: bool = False):
        
        if signed:
            qmin = -(n_levels // 2)
            qmax = qmin + n_levels - 1
        else:
            qmin = 0
            qmax = n_levels - 1
        if asymmetric:
            zero_point = asymmetric_zero_point(scale, MNIST_stats['quantize']['min'], MNIST_stats['quantize']['max'], qmin, qmax)
        else:
            zero_point = 0

        if not dequantize:
            super(MNISTQuantize, self).__init__(lambda x: torch.clip((x / scale).round() + zero_point, qmin, qmax))
        else:
            super(MNISTQuantize, self).__init__(lambda x: (x - zero_point) * scale)


class MNISTNormalize(transforms.Normalize):
    def __init__(self):
        super(MNISTNormalize, self).__init__(**MNIST_stats['normalize'])

def load_MNIST_dataset(path_data: os.PathLike, train: bool, quantize: bool = False, fake_quantize: bool = False, signed: bool = True, asymmetric: bool = False) -> torch.utils.data.Dataset:
    """ load MNIST dataset and specify transformation to apply """
    
    # This sequence of transforms exactly matches the LPDNN preprocessor
    transform_list = [transforms.PILToTensor()]
    transform_list += [transforms.Resize((32,32), interpolation=transforms.InterpolationMode.BICUBIC)]
    transform_list += [transforms.Lambda(lambda x: x / 255)]
    transform_list += [MNISTNormalize()]
                
    if quantize or fake_quantize:
        transform_list += [MNISTQuantize(scale=MNIST_stats['quantize']['scale'], n_levels=256, signed=signed, asymmetric=asymmetric)]
    if fake_quantize:
        transform_list += [MNISTQuantize(scale=MNIST_stats['quantize']['scale'], n_levels=256, signed=signed, asymmetric=asymmetric, dequantize=True)]

    transform = transforms.Compose(transform_list)

    data_set = torchvision.datasets.MNIST(root=path_data, train=train, download=False, transform=transform)

    return data_set

def train_loop(dataloader, model, loss_fn, optimizer, device):
    """ Normal train loop """

    size = len(dataloader.dataset)
    for batch, (X, y) in enumerate(dataloader):
        # Compute prediction and loss
        X = X.to(device)
        y = y.to(device)
        y_pr = model(X)
        loss = loss_fn(y_pr, y)

        # Backpropagation
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        
        if batch % 100 == 0:
            loss, current = loss.item(), batch * len(X)
            print(f"loss: {loss:>7f}  [{current:>5d}/{size:>5d}]")


def test_loop(dataloader, model, loss_fn, device):
    """ Normal test loop """

    size = len(dataloader.dataset)
    num_batches = len(dataloader)
    test_loss, correct = 0, 0

    with torch.no_grad():
        for X,y in dataloader:
            X = X.to(device)
            y = y.to(device)
            y_pr = model(X)
            test_loss += loss_fn(y_pr, y).item()
            correct += (y_pr.argmax(1) == y).type(torch.float).sum().item()

    test_loss /= num_batches
    correct /= size
    print(f"Test Error: \n Accuracy: {(100*correct):>0.1f}%, Avg loss: {test_loss:>8f} \n")

def test_loop_debug(dataloader, model, loss_fn, device):

    size = len(dataloader.dataset)
    num_batches = len(dataloader)
    test_loss, correct = 0, 0

    with torch.no_grad():
        for batch, (X,y) in enumerate(dataloader):
            X = X.to(device)
            y = y.to(device)
            y_pr = model(X)
            test_loss += loss_fn(y_pr, y).item()
            correct += (y_pr.argmax(1) == y).type(torch.float).sum().item()
            if batch % 100 == 0:
                print(y_pr)

    test_loss /= num_batches
    correct /= size
    print(f"Test Error: \n Accuracy: {(100*correct):>0.1f}%, Avg loss: {test_loss:>8f} \n")
