import torch
import torch.nn as nn
from lenet5 import LeNet5, load_MNIST_dataset, test_loop
from lenet5_head_rewriter import LeNet5HeadRewriter

import quantlib.editing.graphs as qg
import quantlib.editing.editing as qe
import quantlib.algorithms as qa

from quantlib.algorithms.qmodules.qmodules.qmodules import _QModule, _QLinear, _QActivation
from quantlib.editing.graphs.fx import FXOpcodeClasses
from quantlib.backends.lpdnn import F2FPACTLPDNNConverter 

#################################################
#       Utils and printing functions            #
#################################################

device = torch.device(torch.cuda.current_device()) if torch.cuda.is_available() else torch.device('cpu')
path_data = "/scratch/msc22h1/quantlab/systems/MNIST/data/"
MNIST_stats = {'normalize': {'mean': (0.1309,), 'std':  (0.3087,)}, 'quantize': {'scale': 0.021994957700371742}}

def print_state(g):
    print(f"PRINTING STATE\n----------------------------------------------------------")
    for node in g.graph.nodes:
        if node.op == "call_module":
            print(f"{node.op} - {node.name} - {node.target} - {g.get_submodule(node.target)} - {type(g.get_submodule(node.target))}")
        else:
            print(f"{node.op} - {node.name} - {node.target}")

def print_meta(g):
    for node in g.graph.nodes:
        print(node.name, node.meta)

def print_annotations(g):
    for node in g.graph.nodes:
        if node.op == "call_module":
            module = g.get_submodule(node.target)
            print(node.name, module)
            if type(module) != type(qg.nn.EpsTunnel(torch.ones([]))):
                print(node.meta['tensor_meta'].shape, node.meta['eps'])
        else:
            print(node.name, node.target)
            print(node.meta['tensor_meta'].shape, node.meta['eps'])

def print_epses(g):
    for node in g.graph.nodes:
        if node.op == "call_module":
            m = g.get_submodule(node.target)
            print(f"{m}")
            if type(m) == type(qg.nn.EpsTunnel(torch.ones(1))):
                print(f"Eps_in = {m.eps_in} - Eps_out = {m.eps_out}")
            elif node.meta:
                print(f"Eps = {node.meta['eps']}")
        else:
            print(f"{node.op} - {node.target}")

def print_lw(g):
    g_lw = qg.lw.quantlib_traverse(g)
    g_lw.show()


# Dataset
valid_set = load_MNIST_dataset(path_data, train=False, quantize=False)

# Load PACT fake-quantized network
net_fq = torch.load("logs/lenet5_fq_maxp.pth")
net_fq.to(device)
#################################################
#       Inspect fake quantised network          #
#################################################

for node in net_fq.graph.nodes:
    print(f"\nop: {node.op} - name: {node.name} - target: {node.target}")
    if node.op in FXOpcodeClasses.CALL_MODULE.value:
        m = net_fq.get_submodule(node.target)
        print(m)
        print(m.__class__.__bases__[1].__name__)
        #if isinstance(m,_QModule):
            #if isinstance(m, _QLinear) or isinstance(m, _QActivation):
                #print(f"scale: {m.scale} - zero: {m.zero}")    
                #print(f"Learnable bounds: {m._pact_learnable_bounds}")
                #print(f"clip_lo = {m.clip_lo.data}, clip_hi = {m.clip_hi.data}")
            #else:
                #print(f"scale: {m.scale} - zero: {m.zero}")
                #print(m.scale)

# To check if clip_lo bound is updated in linear layers by QL
# Load FP32 network
#net = LeNet5(10, pool_type='MaxPool')
# Load pretrained weights
#net.load_state_dict(torch.load("logs/lenet5_fp32_mp_w.pth"))
#conv1 = net.get_submodule("feature_extractor.0")
#print(f"Conv1 min: {conv1.weight.min()}, Conv1 max: {conv1.weight.max()}")
#################################################
#       Test fake quantised network             #
#################################################
"""
Xt, yt = valid_set[0]
Xt = Xt.unsqueeze(0)
Xt = Xt.to(device)
y_pr = net_fq(Xt)
y_pr_int = y_pr.argmax(axis=1)
print(f"Predicted: {yt}   -   True: {y_pr_int}")
print(f"Net fq output: {y_pr}")
"""
