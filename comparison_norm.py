#!/usr/bin/env python3
import argparse
import json
from collections import OrderedDict

import numpy as np
import matplotlib
import matplotlib.pyplot as plt


class Colors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def layout_tuple(dim, layout):
    if layout == '' or layout == 'NCHW' or len(dim) != 4:
        return tuple(dim)
    elif layout == 'NHWC':
        return (dim[0], dim[2], dim[3], dim[1])
    else:
        assert False, 'Unknown layout: ' + layout


class LayerDump(object):
    def __init__(self, output):
        """ Constructor """
        self.dim = output['dim']
        self.data = output['data']
        self.array = np.array(self.data)
        self.layout = output['info'] if 'info' in output else ''
        self.tensor = np.reshape(self.array, layout_tuple(self.dim, self.layout))

    def transpose(self, order):
        self.tensor = self.tensor.transpose(order)
        self.array = self.tensor.flatten()
        self.data = self.array.tolist()


def parse_args():
    """ Parse arguments """
    parser = argparse.ArgumentParser("Face_recogniton fingerprint euclidean distance")
    parser.add_argument('dump0', help='First layer dump to compare')
    parser.add_argument('dump1', help='Second layer dump to compare')
    parser.add_argument('--verbose', help='Dump blob content of the specified layer name')
    parser.add_argument('--layer', help='Compare feature maps of the specified layer name')
    parser.add_argument(
        '--fm',
        nargs='+',
        type=int,
        help='Compare feature maps of the specified layer')
    parser.add_argument('--out', help='Generate the fm heatmap in the specified file')
    parser.add_argument('--threshold', default="1.0", help='Error threshold')
    parser.add_argument('--data', default=None, help='Show data 0 or 1 instead of diff')
    parser.add_argument('--randomize-dumps', action='store_true', help='Produce differences from randomized dump in the same range as the input dumps')
    return parser.parse_args()


def get_matching_name(name: str, layer_dict):
    if name in layer_dict:
        # Exact match
        return name

    match_count = 0
    matching_name = None
    layer_names = layer_dict.keys()
    for layer_name in layer_names:
        if name in layer_name or layer_name in name:
            # A substring
            match_count += 1
            matching_name = layer_name

    if match_count > 1:
        # Ignore match if not unique
        matching_name = None

    return matching_name

def randomize_dumps_same_range(args, ld0: LayerDump, ld1: LayerDump):

    ld0_min = np.amin(ld0.array)
    ld0_max = np.amax(ld0.array)
    ld1_min = np.amin(ld1.array)
    ld1_max = np.amax(ld1.array)
    shape = ld0.array.shape
    
    iterations = 1000
    norms = []
    for i in range(iterations):
        ld0_new_array = np.random.uniform(low=ld0_min,high=ld0_max,size=shape) 
        ld1_new_array = np.random.uniform(low=ld1_min,high=ld1_max,size=shape)
    
        diff = ld0_new_array - ld1_new_array
        norm = np.sqrt(np.dot(diff, diff))
        norms.append(norm)
    
    avg_norm = sum(norms) / iterations

    threshold = float(args.threshold)
    if avg_norm > threshold:
        color = Colors.FAIL
    else:
        color = Colors.WARNING if avg_norm > 0.1 else Colors.OKGREEN
    print("Randomized dumps: " + color +
        "{0:.5f}".format(avg_norm) + Colors.ENDC)

def main():
    """ main program """
    args = parse_args()
    threshold = float(args.threshold)

    with open(args.dump0) as json_file:
        result0 = json.load(json_file)
    with open(args.dump1) as json_file:
        result1 = json.load(json_file)

    layers0 = result0['layers']
    layers1 = result1['layers']
    if len(layers0) == len(layers1):
        print("Layers: " + str(len(layers0)))
    else:
        print("Layers: " + str(len(layers0)) + " != " + str(len(layers1)))

    # Use ordered dict so layers are shown in order
    layer_dict_0 = OrderedDict([(l['name'], l) for l in layers0])
    layer_dict_1 = {l['name']: l for l in layers1}


    for layer_name in layer_dict_0.keys():
        print(layer_name + ": ", end='')
        ld0 = LayerDump(layer_dict_0[layer_name]['output'])

        # Only compare layers that appear in both dumps

        layer_name_1 = get_matching_name(layer_name, layer_dict_1)
        if not layer_name_1:
            print(ld0.layout + str(ld0.dim) + ": --")
            continue
        if layer_name_1 != layer_name:
            print(layer_name_1 + ": ", end='')

        ld1 = LayerDump(layer_dict_1[layer_name_1]['output'])
        if ld0.dim != ld1.dim and ld0.dim[3] != 1:
            print(Colors.FAIL + "DIM ERROR: " + str(ld0.dim) + " != " + str(ld1.dim))
        else:
            if len(ld0.dim) == 4:
                if ld0.layout == 'NHWC' and ld1.layout == 'NCHW':
                    ld1.transpose((0, 2, 3, 1))
                elif ld0.layout == 'NCHW' and ld1.layout == 'NHWC':
                    ld1.transpose((0, 3, 1, 2))
                elif ld0.layout != ld1.layout:
                    print(Colors.WARNING + "Unsupported layout conversion " + Colors.ENDC, end='')

            if args.randomize_dumps:
                randomize_dumps_same_range(args,ld0,ld1)

            try:
                diff = ld0.array - ld1.array
                norm = np.sqrt(np.dot(diff, diff))
            except Exception as e:
                print(Colors.FAIL + "DIFF FAILED: " + Colors.ENDC + str(e) + '   ', end='')
                norm = 9999

            if norm > threshold:
                color = Colors.FAIL
            else:
                color = Colors.WARNING if norm > 0.1 else Colors.OKGREEN
            print(ld0.layout + str(ld0.dim) + ld1.layout + ":   " + color +
                  "{0:.5f}".format(norm) + Colors.ENDC)
            if args.verbose == layer_name:
                # print(json.dumps(ld0.tensor.tolist()))
                # print(json.dumps(ld1.tensor.tolist()))
                print(ld0.tensor)
                print(ld1.tensor)
                return

            
if __name__ == '__main__':
    main()
