# Load model and quantize it with PACT/SAWB. Uses the new version of Quantlib
import os
import torch
import torch.nn as nn
import torch.fx as fx
from lenet5 import LeNet5, load_MNIST_dataset, train_loop, test_loop

import quantlib.editing.graphs as qg
from quantlib.editing.graphs.lightweight.rules.filters import TypeFilter
import quantlib.editing.editing as qe
import quantlib.algorithms as qa
import quantlib.backends.lpdnn as lpdnn

def debug(g: fx.GraphModule):
    for n in g.graph.nodes:
        if n.op == "call_module":
            m = g.get_submodule(n.target)
            if isinstance(m, qa.qalgorithms.qatalgorithms.pact.qmodules._PACTModule):
                print(m)
                print(f"scale = {m.scale}, zero = {m.zero}, clip_lo = {m.clip_lo.data}, clip_hi = {m.clip_hi.data}")
            if n.name == "classifier_2":
                print(m.qweight.data)

# get device
device = torch.device(torch.cuda.current_device() if torch.cuda.is_available() else "cpu")
print(f"Device: {device}")

# Load FP32 network
net = LeNet5(10, pool_type='MaxPool')
net = net.to(device)

# Initialize weights randomly
#net.apply(LeNet5.init_weights_uniform)

# Load pretrained weights
net.load_state_dict(torch.load("logs/lenet5_fp32_mp_w.pth"))

# Path to train/test data
path_data = "/scratch/msc22h1/quantlab/systems/MNIST/data/" 

# Needed datasets
valid_set = load_MNIST_dataset(path_data, train=False, fake_quantize=True, signed=False)
valid_loader = torch.utils.data.DataLoader(valid_set, batch_size=64)
train_set = load_MNIST_dataset(path_data, train=True, fake_quantize=True, signed=False)
train_loader = torch.utils.data.DataLoader(train_set, batch_size=64)

#################################################
#              F2F conversion                   #
#################################################

# trace
net.eval()
net_fp = qg.fx.quantlib_symbolic_trace(root=net)

# convert using f2f converter which targets LPDNN
#f2fconverter = qe.f2f.F2F8bitPACTConverter()
f2fconverter = lpdnn.F2FPACTLPDNNConverter()
net_fq = f2fconverter(net_fp)

net_fq_lw = qg.lw.quantlib_traverse(net_fq) 
net_fq_lw.show()

# Save model with shape
#print("Saving fq model...")
#net_fq = net_fq.to(torch.device("cpu"))
#torch.save(net_fq, "pact_net.pth")


#################################################
#       Before training: Observing              #
#################################################

loss_fn = torch.nn.CrossEntropyLoss()

#net_fq.to(device)
net_fq.eval()


for m in net_fq.modules():
    if isinstance(m, tuple(qa.qalgorithms.qatalgorithms.pact.NNMODULE_TO_PACTMODULE.values())):
        m.start_observing()

test_loop(valid_loader, net_fq, loss_fn, torch.device('cpu'))

for m in net_fq.modules():
    if isinstance(m, tuple(qa.qalgorithms.qatalgorithms.pact.NNMODULE_TO_PACTMODULE.values())):
        m.stop_observing()

net_fq.to(device)
test_loop(valid_loader, net_fq, loss_fn, device)
#################################################
#       Train and test fp net                   #
#################################################
# Train fake quantized network (Quantsation Aware Fine Tuning)
optimizer = qa.qalgorithms.qatalgorithms.pact.PACTAdam(net_fq, pact_decay=0.001, lr=0.001)
net_fq.train()
print("*** Training QAFT ***")
for e in range(4):
    train_loop(train_loader, net_fq, loss_fn, optimizer, device)
    test_loop(valid_loader, net_fq, loss_fn, device)
net_fq.eval()

""" Test fake quantized network
Xt, yt = test_loader.dataset[0]
Xt = Xt.unsqueeze(0)
Xt = Xt.to(device)
y_pr = pact_net(Xt)
y_pr = y_pr.to(device)
y_pr_int = y_pr.argmax(axis=1)
print(f"Predicted: {yt}   -   True: {y_pr_int}")
"""

# Save model with shape
#print("Saving fq network...")
#net_fq = net_fq.to(torch.device("cpu"))
#torch.save(net_fq, "logs/lenet5_fq_maxp.pth")


