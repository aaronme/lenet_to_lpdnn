# LeNet5 to LPDNN
Creates LeNet5 implementation, fake-quantizeis with PACT/SAWB, and exports it to an ONNX using the QuantLab-to-qONNX converter.
