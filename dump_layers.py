from typing import Dict, OrderedDict
import json
import torch
import torch.nn as nn
import torch.fx as fx
from lenet5 import LeNet5, load_MNIST_dataset, test_loop
import quantlib.editing.graphs as qg
import quantlib.algorithms as qa
from quantlib.backends.lpdnn import F2FPACTLPDNNConverter

def create_feature_extractor(g: fx.GraphModule, return_nodes: Dict = None):

    # remove existing output nodes
    orig_output_nodes = []
    for n in reversed(g.graph.nodes):
        if n.op == 'output':
            orig_output_nodes.append(n)
    assert len(orig_output_nodes)
    for n in orig_output_nodes:
        g.graph.erase_node(n)

    nodes = [n for n in g.graph.nodes]
    output_nodes = OrderedDict()
    
    for n in reversed(nodes):
        for query in return_nodes:
            if n.name == query:
                output_nodes[return_nodes[query]] = n
                return_nodes.pop(query)
                break
    
    output_nodes = OrderedDict(reversed(list(output_nodes.items())))

    with g.graph.inserting_after(nodes[-1]):
        g.graph.output(output_nodes)

    g.graph.eliminate_dead_code()
    g.recompile()

    return g

def to_json(features: Dict, layer_names: Dict, filepath: str):

    # check same keys
    assert features.keys() == layer_names.keys()
    
    output_dict = {"name":"model"}
    layers = []

    for n in features:
        name = layer_names[n]
        dim = list(features[n].shape)
        data = features[n].flatten().tolist()
        data_dict = {"data":data, "dim":dim, "info": "NCHW"}
        layer_dict = {"name":name, "output":data_dict}
        layers.append(layer_dict)

    output_dict["layers"] = layers
    
    with open(filepath, "w") as f:
        json.dump(output_dict, f, indent=0)


def print_quant_params(g: fx.GraphModule):
    for n in g.graph.nodes:
        if n.op == 'call_module':
            m = g.get_submodule(n.target)
            if isinstance(m, qa.qmodules.qmodules.qmodules._QModule):
                print(f"{n.name}: scale = {m.scale}, zero = {m.zero}")

# Dataset
path_data = "/scratch/msc22h1/quantlab/systems/MNIST/data/"
valid_set = load_MNIST_dataset(path_data, train=False, fake_quantize=True, signed=False, asymmetric=False)

# Load PACT fake-quantized network
net_fq = torch.load("logs/lenet5_fq_maxp.pth")

#####################################################
#               dump layers outputs                 #
#####################################################
#print_quant_params(net_fq)

# Dictionary to select which layers to dump and which name to give them.
# We leave the same names
return_nodes ={
        'feature_extractor_0':'feature_extractor_0',
        'feature_extractor_1':'feature_extractor_1',
        'feature_extractor_2':'feature_extractor_2',
        'feature_extractor_3':'feature_extractor_3',
        'feature_extractor_4':'feature_extractor_4',
        'feature_extractor_5':'feature_extractor_5',
        'feature_extractor_6':'feature_extractor_6',
        'feature_extractor_7':'feature_extractor_7',
        'classifier_0':'classifier_0',
        'classifier_1':'classifier_1',
        'classifier_2':'classifier_2',
        }
feature_extractor = create_feature_extractor(net_fq, return_nodes)
x, y = valid_set[0]
x = x.unsqueeze(0)
extracted_features = feature_extractor(x)
features = {'x': x}
features.update(extracted_features)
torch.set_printoptions(threshold=10_000, linewidth=250)

# This dictionary allow to match the layers names with the ones produced by the LPDNN dumper.
layer_names ={
        'x':'input',
        'feature_extractor_0':'conv1',
        'feature_extractor_1':'feature_extractor_0_output',
        'feature_extractor_2':'feature_extractor_2_output',
        'feature_extractor_3':'conv2',
        'feature_extractor_4':'feature_extractor_3_output',
        'feature_extractor_5':'feature_extractor_5_output',
        'feature_extractor_6':'conv3',
        'feature_extractor_7':'feature_extractor_6_output',
        'classifier_0':'linear1',
        'classifier_1':'classifier_0_output',
        'classifier_2':'classifier_2_output',
        }
to_json(features, layer_names, "dumps/test.json")
