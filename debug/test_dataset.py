import torch
import torch.nn as nn
import torchvision
import torchvision.transforms as transforms
from lenet5 import LeNet5, test_loop_debug, load_MNIST_dataset, MNIST_stats, MNISTQuantize, MNISTNormalize

torch.set_printoptions(threshold=10_000, linewidth=250, precision=3)
device = torch.device(torch.cuda.current_device()) if torch.cuda.is_available() else torch.device('cpu')
path_data = "/scratch/msc22h1/quantlab/systems/MNIST/data/"


#transform_list = [transforms.PILToTensor()]
#transform_list += [transforms.Resize((32,32), interpolation=transforms.InterpolationMode.BICUBIC)]
#transform_list += [transforms.Lambda(lambda x: x / 255)]
#transform_list += [MNISTNormalize()]
#scale = 1.0/256.0
#transform_list += [MNISTIntegerize(scale=scale, n_levels=256, signed=False)]

#transform = transforms.Compose(transform_list)

#valid_set = torchvision.datasets.MNIST(root=path_data, train=False, download=False, transform=transform)
valid_set = load_MNIST_dataset(path_data, train=False, quantize=True, signed=False, asymmetric=False)


x, y = valid_set[0]
print(x.shape)
print(x, x.dtype)
print(y)




#torchvision.io.write_png(x, "0_valid_set.png")
#valid_set = load_MNIST_dataset(path_data, train=False)
#valid_loader = torch.utils.data.DataLoader(valid_set, batch_size=16)

# Inspect weights trained with unnormalized dataset
#net = LeNet5(10, pool_type='MaxPool')
#net.to(device)
#net.load_state_dict(torch.load("../logs/lenet5_fp32_mp_w_unnormalized.pth"))

#for m in net.submodules():
#    if isinstance(m, nn.Linear) or isinstance(m, nn.Conv2d):
#        print(m.weight)


#loss_fn = nn.CrossEntropyLoss()
#test_loop_debug(valid_loader, net, loss_fn, device)

