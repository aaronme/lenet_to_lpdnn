import torch
import torch.nn as nn
#from onnxruntime.quantization import quantize_static
#mport quantlib.editing.graphs as qg
#from lenet5 import LeNet5, load_MNIST_dataset

def print_state(g):
    print(f"PRINTING STATE\n----------------------------------------------------------")
    for node in g.graph.nodes:
        if node.op == "call_module":
            print(f"{node.op} - {node.name} - {node.target} - {g.get_submodule(node.target)} - {type(g.get_submodule(node.target))}")
        else:
            print(f"{node.op} - {node.name} - {node.target}")

class Dummy(nn.Module):
    def __init__(self):
        super().__init__()
        self.lin = nn.Linear(84,10)
    def forward(self, x):
        x = self.lin(x)
        x = torch.clip(x, torch.Tensor([0]), torch.Tensor([255]))
        return x

model = nn.Sequential()
module = nn.AvgPool2d(kernel_size=6)
print(f"AvgPool2d module kernel size: {module.kernel_size}")
