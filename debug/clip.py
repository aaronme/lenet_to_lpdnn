import torch
import torch.nn as nn


class ClipTest(nn.Module):
    def __init__(self, n_levels: torch.Tensor, zero: torch.Tensor):
        
        super(ClipTest,self).__init__()
        self.register_buffer('n_levels', n_levels)
        self.register_buffer('zero', zero)
        print(self.n_levels)
    def forward(self, x: torch.Tensor) -> torch.Tensor:
        x = torch.clip(x,float(self.zero),float(self.n_levels))
        return x

n_levels = torch.Tensor([256])
zero = torch.Tensor([0])
m = ClipTest(n_levels,zero)
x = torch.Tensor([23,30])
print(x)
x = m(x)
print(x)


print("Exporting to onnx...")
debug_path = "../onnx/debug.onnx"
torch.onnx.export(m.to(torch.device('cpu')),
                      x,
                      debug_path,
                      export_params=True,
                      opset_version=10,
                      do_constant_folding=True)

