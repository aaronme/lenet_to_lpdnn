import torch
import quantlib.editing.fx as qfx
from quantlib.editing.fx.passes.pact.pact_util import PACT_symbolic_trace

input_sample = torch.randn(1,1,32,32)
input_eps = torch.Tensor([0.0078])

#Trace shape and annotate to see eps tensor dimensions
pact_network = torch.load("../logs/pact_net.pth", map_location=torch.device("cpu"))
tracer = qfx.passes.general.RetracePass(PACT_symbolic_trace)
sp = qfx.passes.general.ShapePropPass(input_sample.size())
annotator = qfx.passes.eps.AnnotateEpsPass(input_eps)
gm = tracer(pact_network)
gm = sp(gm)
gm = annotator(gm)
"""
for node in gm.graph.nodes:
    print(f"name: {node.name} --- target: {node.target}")
    if isinstance(node.meta['quant'].eps_in, list):
        print(f"eps_in: {node.meta['quant'].eps_in[0][0].size()}, eps_out: {node.meta['quant'].eps_out.size()}")
    else:
        print(f"eps_in: {node.meta['quant'].eps_in.size()}, eps_out: {node.meta['quant'].eps_out.size()}")
"""

# Check eps of corrupted module by extracting them as in bn_act_replacement_fn
for node in gm.graph.nodes:
    if node.name == "feature_extractor_1":
        eps_in = qfx.passes.eps.extract_eps(node.meta['quant'].eps_in).cpu().clone().detach().squeeze()
        eps_out = node.meta['quant'].eps_out.cpu().clone().detach().squeeze()
        ns_eps_in = qfx.passes.eps.extract_eps(node.meta['quant'].eps_in).cpu().clone().detach()
        ns_eps_out = node.meta['quant'].eps_out.cpu().clone().detach()
        print(f"eps_in: {type(eps_in)}, {eps_in.size()}")
        print(f"eps_out: {type(eps_out)}, {eps_out.size()}")
        print(f"Not squeezed eps_in: {type(ns_eps_in)}, {ns_eps_in.size()}")
        print(f"Not squeezed eps_out: {type(ns_eps_out)}, {ns_eps_out.size()}")

"""#Meta dict of tq network is empty. Why? Shouldn't the eps be annotated
tq_pact_network = torch.load("../tq_pact_net.pth", map_location=torch.device("cpu"))
for node in tq_pact_network.graph.nodes:
    print(f"name: {node.name} --- target: {node.target}")
    print(f"meta: {node.meta}")
"""
""" The shape propagation pass on the tq network won't work because of the corrupted module
tq_sp = qfx.passes.general.ShapePropPass(input_sample.size())
tq_gm = tq_sp(tq_pact_network)
for node in tq_gm.graph.nodes:
    print(f"name: {node.name} --- target: {node.target}")
    print(f"meta: {node.meta}")
"""
