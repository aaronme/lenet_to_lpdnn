import torch
import torch.nn as nn
from  torch.fx.passes.shape_prop import ShapeProp
from subgraph import extract_network_up_to


tq_pact_network = torch.load("../tq_pact_net.pth", map_location=torch.device("cpu"))
sample_input = torch.randn(8,1,32,32)

"""Find the module that generates the error when executed
for node in tq_pact_network.graph.nodes:
    print(f"node.op = {node.op} --- node.name = {node.name} --- node.target = {node.target}")

    if node.op == "call_module":

        #submodule = tq_pact_network.get_submodule(node.target)
        subgraph = extract_network_up_to(node.target, tq_pact_network)

        # Execute subgraph: the program will stop at the first subgraph containing the corrupted node.
        subgraph(sample_input)
        print("EXECUTED")
"""
### THE CORRUPTED MODULE IS: feature_extractor._QL_REPLACED__INTEGERIZE_UNSIGNED_ACT_PASS_0 ###  
### THE MODULE BEFORE IS: feature_extractor._QL_REPLACED__INTEGERIZE_PACT_CONV2D_PASS_0

print("\nTQ pact network nodes\n------------------------")
for node in tq_pact_network.graph.nodes:
    print(f"op: {node.op} --- name: {node.name} --- target: {node.target}")
    if node.op == "call_module":
        print(f"module: {tq_pact_network.get_submodule(target=node.target)}")

corrupted_module = tq_pact_network.get_submodule(target="feature_extractor._QL_REPLACED__INTEGERIZE_UNSIGNED_ACT_PASS_0")
print("\nCorrupted module (UNSIGNED_ACT_PASS_0 - RequantShift()) buffers\n------------------------------------------------------")
for buf_name, buf in corrupted_module.named_buffers():
    print(f"{buf_name} - {buf.size()} - {type(buf)} - {buf}")


# Parent module shapes
parent_module = tq_pact_network.get_submodule(target="feature_extractor._QL_REPLACED__INTEGERIZE_PACT_CONV2D_PASS_0")
# Get subgraph until parent module, trace it and ouput shapes
parent_subgraph = extract_network_up_to("feature_extractor._QL_REPLACED__INTEGERIZE_PACT_CONV2D_PASS_0", tq_pact_network)
ShapeProp(parent_subgraph).propagate(sample_input)
print("\nShapes until parent module\n--------------------------")
for node in parent_subgraph.graph.nodes:
    print(f"node.name = {node.name}, node.target = {node.target}")
    if node.name == "output":
        print(f"node.meta: {node.meta}")
    else:    
        print(f"node.meta['tensor_meta'] dtype and shape: {node.meta['tensor_meta'].dtype}, {node.meta['tensor_meta'].shape}")

# As per RequantShift implementation (see its forward method): x = x * self.mul try to multiply out tensor of parent node  Torch.Size([8, 6, 28, 28]) with corrupted modul 'mul' buffer
parent_output = parent_subgraph(sample_input)
print(f"\nparent_output: {type(parent_output)}, {parent_output[0].size()}")
mul = corrupted_module.get_buffer(target="mul")
print(f"multiply with corrupted node 'mul' buffer, {type(mul)}")
y = parent_output[0] * mul



#pact_network = torch.load("../logs/pact_net.pth", map_location=torch.device("cpu"))

""" Buffers of FQ counterpart of the corrupted node
print("\nBuffers of the fake quantized counterpart of the corrupted module\n--------------------------------------------")
fq_corrupted_module = pact_network.get_submodule(target="feature_extractor.1")
for buf_name, buf in fq_corrupted_module.named_buffers():
    print(f"{buf_name} - {buf.size()} - {type(buf)} - {buf}")
"""

"""# Propagate shapes in FQ network
gm = torch.fx.symbolic_trace(pact_network)
ShapeProp(gm).propagate(sample_input)
for node in gm.graph.nodes:
    print(f"node.name = {node.name}, {node.meta['tensor_meta'].dtype}, {node.meta['tensor_meta'].shape}")
"""
#for node in tq_gm.graph.nodes:
    #print(node.name, node.target)
    #submodule = tq_gm.get_submodule(target=node.target)
    #print(submodule)
    #sub_tq_gm = extract_network_up_to(node.name, tq_gm)
    #ShapeProp(sub_tq_gm).propagate(sample_input)


#conv_m = tq_gm.get_submodule(target="feature_extractor._QL_REPLACED__INTEGERIZE_PACT_CONV2D_PASS_0")
#print(conv_m.weight.shape)


