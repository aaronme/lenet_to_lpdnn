# Perform f2t
from typing import Tuple
import torch
import torch.nn as nn
from torch.fx.passes.shape_prop import ShapeProp
import torchvision.transforms as transforms
from lenet5 import LeNet5, load_MNIST_dataset
import quantlib.algorithms as qa
import quantlib.editing.fx as qfx

##### Helper functions #####

def get_input_range(data_loader: torch.utils.data.DataLoader) -> Tuple[float, float]:
    """Traverse the available data set to get the empirical range of input pixels."""
    min_ = 0.0
    max_ = 0.0

    for x, _ in data_loader:
        min_ = min(min_, x.min().item())
        max_ = max(max_, x.max().item())
    
    return min_, max_


def add_quantisation_transform(data_loader: torch.utils.data.DataLoader, n_levels: int, min_: float, max_: float) -> float:
    
    quantiser = qa.pact.PACTAsymmetricAct(n_levels=n_levels, symm=True, learn_clip=False, init_clip='max', act_kind='identity')

    clip_lo, clip_hi       = qa.pact.util.almost_symm_quant(torch.Tensor([max(abs(min_), abs(max_))]), n_levels)
    quantiser.clip_lo.data = clip_lo
    quantiser.clip_hi.data = clip_hi
    
    quantiser.started |= True
    
    transform_list  = []
    transform_list += [data_loader.dataset.transform]
    transform_list += [quantiser]
    transform_list += [transforms.Lambda(lambda x: x / quantiser.get_eps())]
    
    data_loader.dataset.transform = transforms.Compose(transform_list)
    
    return quantiser.get_eps()

def f2t_convert(dataloader: torch.utils.data.DataLoader, input_eps: float, network: nn.Module) -> nn.Module:

    network.eval()
    network_traced = qfx.passes.pact.PACT_symbolic_trace(network)

    x, _ = dataloader.dataset[0]
    x = x.unsqueeze(0)
    fake2true_converter = qfx.passes.pact.IntegerizePACTNetPass(shape_in=x.shape, eps_in=input_eps, D=2**19)

    return fake2true_converter(network_traced)


##############################


# Path of data
path_data = "~/quantlab/systems/MNIST/data/" 

# Datasets
test_set = load_MNIST_dataset(path_data, train=False)

# bs and test dataloader
batch_size = 64
test_loader = torch.utils.data.DataLoader(test_set, batch_size=batch_size)

"""
# Check dataloader
X, y = test_loader.dataset[0]
print(f"X shape: {X.size()}")
print(f"X[:,9,:] = {X[:,9,:]}, type:{type(X[0,0,0].item())}\ny = {y}, type: {type(y)}")
"""

# Add transform to dataloader to integerize inputs
min_, max_ = get_input_range(test_loader)
input_eps = add_quantisation_transform(test_loader, 256, min_, max_)
print(f"Min, Max: {min_},{max_}")
print(f"Input_eps = {input_eps}")

# Load pact network
pact_network = torch.load("../logs/pact_net_maxp.pth", map_location=torch.device("cpu")) 


# F2T
tq_pact_network = f2t_convert(test_loader, input_eps, pact_network)

#print("True quantized pact network:\n")
#for module in tq_pact_network.modules():
#    print(module)

# Check if prediction are correct against ground truth
X, y = test_loader.dataset[1]
X = X.unsqueeze(0)
y_pr = tq_pact_network(X)
y_pr_int = y_pr.argmax(axis=1)
print(f"Predicted: {y}   -   True: {y_pr_int}")

# Save TQ pact network 
torch.save(tq_pact_network, "../logs/tq_lenet5_maxp_oq.pth")


# Export standard onnx
# Problem: onnx insterts unnecessary pads which are not importable in lpdnn. See this issue: https://github.com/pytorch/pytorch/issues/56218 
# Solution (Commented in the issue comments): pass "count_include_pad=False" to "nn.AvgPool2d"

print("Exporting to onnx...")
onnx_path = "../onnx/lenet5_ql_integerised_maxp_oq.onnx" 
torch.onnx.export(tq_pact_network.to('cpu'),
                      X,
                      onnx_path,
                      export_params=True,
                      opset_version=10,
                      do_constant_folding=True)

"""
# Shape prop. Checking what happen to the Pool layers
tq_gm = torch.fx.symbolic_trace(tq_pact_network)
ShapeProp(tq_gm).propagate(X)
for node in tq_gm.graph.nodes:
    print(f"name: {node.name}, target: {node.target}, op: {node.op}")
    if node.op == "call_module":
        submodule = tq_gm.get_submodule(target=node.target)
        print(f"Module: {submodule}")
    print(f"meta['tensor_meta'].shape: {node.meta['tensor_meta'].shape}")
    #if "tensor_meta" in node.meta:
    #    print(f"node.meta['tensor_meta'] dtype and shape: {node.meta['tensor_meta'].dtype}, {node.meta['tensor_meta'].shape}")
    #else:    
    #    print(f"node.meta: {node.meta}")
"""
