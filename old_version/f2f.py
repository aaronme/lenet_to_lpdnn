# Train model and quantize it with PACT/SAWB
import os
import torch
import torch.nn as nn
import quantlib.editing.lightweight as qlw
import quantlib.algorithms as qa
from lenet5 import LeNet5, load_MNIST_dataset, train_loop, test_loop, qnt_train_and_test

# get device
device = torch.device(torch.cuda.current_device() if torch.cuda.is_available() else "cpu")

# Model
net = LeNet5(10, pool_type='MaxPool')
net = net.to(device)

# Path of data
path_data = "~/quantlab/systems/MNIST/data/" 

# Datasets
train_set = load_MNIST_dataset(path_data, train=True) 
test_set = load_MNIST_dataset(path_data, train=False)

# bs and dataloaders
batch_size = 64
train_loader = torch.utils.data.DataLoader(train_set, batch_size=batch_size)
test_loader = torch.utils.data.DataLoader(test_set, batch_size=batch_size)

""" test loaders
bx, by = next(iter(train_loader))
print(f"Shape of input: {bx.shape}")
print(f"Shape of label: {by.shape}")
"""

# Training parameters
loss_fn = nn.CrossEntropyLoss()
lr = 0.001 
optimizer = torch.optim.Adam(net.parameters(), lr=lr)
epochs = 2

print("*** Normal Training ***")
for e in range(epochs):
    print(f"Epoch {e+1}\n-----------------------------")
    train_loop(train_loader, net, loss_fn, optimizer, device)
    test_loop(test_loader, net, loss_fn, device)

# Save fp32 network
#print("Saving fp32 network")
#torch.save(net.state_dict(),"lenet5_fp32_w.pth")
#exit()
#################################################
#       quantization with PACT/SAWB             #
#################################################

# Quantization configs
quant_conv2d = \
        {   
            "quantize": "per_channel",
            "init_clip": "sawb_asymm",
            "learn_clip": False,
            "symm_wts": True,
            "tqt": False,
            "n_levels": 256             # 8bit
        }

quant_linear = \
        {   
            "quantize": "per_layer",
            "init_clip": "sawb_asymm",
            "learn_clip": False,
            "symm_wts": True,
            "tqt": False,
            "n_levels": 256             # 8bit
        }

quant_ReLU = \
        {   
            "init_clip": "std",
            "learn_clip": True,
            "nb_std": 3,
            "rounding": False,
            "tqt": False,
            "n_levels": 256             # 8bit
        }

# Instantiate lwg 
lwg = qlw.LightweightGraph(net)

# Filter nodes and assign configs
conv2d_nodes = set([n.name for n in lwg.nodes_list if n.module.__class__.__name__ == "Conv2d"])
linear_nodes = set([n.name for n in lwg.nodes_list if n.module.__class__.__name__ == "Linear"])
relu_nodes = set([n.name for n in lwg.nodes_list if n.module.__class__.__name__ == "ReLU"])

conv2d_config = {}
linear_config = {}
ReLU_config = {}

for n in conv2d_nodes:
    conv2d_config[n] = quant_conv2d
for n in linear_nodes:
    linear_config[n] = quant_linear
for n in relu_nodes:
    ReLU_config[n] = quant_ReLU

name2config = {**conv2d_config, **linear_config, **ReLU_config}

name2type = {n.name: n.module.__class__.__name__ for n in lwg.nodes_list}

# Replacement rules
type2rule = \
        {
                "Conv2d": qlw.rules.pact.ReplaceConvLinearPACTRule,
                "Linear": qlw.rules.pact.ReplaceConvLinearPACTRule,
                "ReLU" : qlw.rules.pact.ReplaceActPACTRule
        }

# Replacement rules application
# name2config.keys() --> (conv2d,linear,relu) layers names
# type2rule[name2type[n]] --> for names in name2config take correct rule object from type2rule bsed on type
# pass (qlw.rules.NameFilter(n), **name2config[n]) to the object constructor --> give a name and a config dict
rules = list(map(lambda n: type2rule[name2type[n]](qlw.rules.NameFilter(n), **name2config[n]), name2config.keys()))
print("*** Quantizing network with spedified configs ***")
lwe = qlw.LightweightEditor(lwg)
lwe.startup()
for rule in rules:
    lwe.set_lwr(rule)
    lwe.apply()
lwe.shutdown()

pact_net = lwe.graph.net
pact_net = pact_net.to(device)

# Save model with shape
#torch.save(pact_net, "pact_net.pth")


### Controllers and training of quantization parameters ###

modules_linear = qa.pact.PACTLinearController.get_modules(pact_net)
modules_act = qa.pact.PACTActController.get_modules(pact_net)

controller_linear = qa.pact.PACTLinearController(modules_linear, {0:["verbose_on", "start"]}, {})
controller_act = qa.pact.PACTActController(modules_act, {0:["verbose_on", "start"]}, {})

loss_fn = nn.CrossEntropyLoss()
optimizer = qa.pact.PACTAdam(pact_net, pact_decay=0.001, lr=0.004)
lr_sched = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer, T_max=1, eta_min=0.00001) # T_max should equal n epochs (check why)
print("*** Training of quantization parameters ***")
qnt_train_and_test(train_loader, test_loader, pact_net, loss_fn, optimizer, lr_sched, [controller_linear, controller_act], 1, device)

"""
# Test fake quantized network
Xt, yt = test_loader.dataset[0]
Xt = Xt.unsqueeze(0)
Xt = Xt.to(device)
y_pr = pact_net(Xt)
y_pr = y_pr.to(device)
y_pr_int = y_pr.argmax(axis=1)
print(f"Predicted: {yt}   -   True: {y_pr_int}")
"""

#print(pact_net)
# Save net with shape
print("Saving pact network")
pact_net = pact_net.to(torch.device("cpu"))
torch.save(pact_net, "../logs/pact_net_maxp.pth")
