# Trains fp32 LeNet5
import os
import torch
import torch.nn as nn
from lenet5 import LeNet5, load_MNIST_dataset, train_loop, test_loop

# get device
device = torch.device(torch.cuda.current_device() if torch.cuda.is_available() else "cpu")

# Model
net = LeNet5(10, pool_type='MaxPool')
net = net.to(device)

# Path of data
path_data = "/scratch/msc22h1/quantlab/systems/MNIST/data/" 

# Datasets
train_set = load_MNIST_dataset(path_data, train=True) 
test_set = load_MNIST_dataset(path_data, train=False)

# bs and dataloaders
batch_size = 64
train_loader = torch.utils.data.DataLoader(train_set, batch_size=batch_size)
test_loader = torch.utils.data.DataLoader(test_set, batch_size=batch_size)

""" test loaders
bx, by = next(iter(train_loader))
print(f"Shape of input: {bx.shape}")
print(f"Shape of label: {by.shape}")
"""

# Training parameters
loss_fn = nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(net.parameters(), lr=0.001)
epochs = 2

""" Train and save
print("*** Normal Training ***")
for e in range(epochs):
    print(f"Epoch {e+1}\n-----------------------------")
    train_loop(train_loader, net, loss_fn, optimizer, device)
    test_loop(test_loader, net, loss_fn, device)

# Save fp32 network
print("Saving fp32 network weights")
torch.save(net.state_dict(),"logs/lenet5_fp32_mp_w.pth")
"""

# load and test
net.load_state_dict(torch.load("logs/lenet5_fp32_mp_w.pth"))
test_loop(test_loader, net, loss_fn, device)
