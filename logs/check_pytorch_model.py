import torch
import argparse

parser = argparse.ArgumentParser('Check pytorch model')
parser.add_argument('-m', '--model', dest='model', type=str, required=True, help='path to .onnx/pytorch model file')

args = parser.parse_args()

g = torch.load(args.model)

for node in g.graph.nodes:
        if node.op == "call_module":
            print(f"{node.op} - {node.name} - {node.target} - {g.get_submodule(node.target)} - {type(g.get_submodule(node.target))}")
            print(node.meta)
        else:
            print(f"{node.op} - {node.name} - {node.target}")
            print(node.meta)
